from app.server_modal_ui import show_servers_list
from curses import ascii
from discord.channel import TextChannel
import asyncio
import curses

from app import user_commands
from app.channel_list_ui import ChannelList
from app.user_list_ui import UserList
from app.message_commands import edit_previous_message
from app.modal_ui import show_help_modal
from app.server_ui import render_current_server, render_messages
from config import config
from tools import logger
from tools.screen_funcs import sc_addstr, sc_getyx, sc_move, sc_refresh


class UI:
    icons = {
        "top_left_corner": "╔",
        "top_right_corner": "╗",
        "horizontal": "═",
        "vertical": "║",
        "bottom_left_corner": "╚",
        "bottom_right_corner": "╝",
        "top_intersection": "╦",
        "right_intersection": "╣",
        "bottom_intersection": "╩",
        "left_intersection": "╠",
        "four_intersection": "╬"
    }

    # Mostly for reference to know what kind of UIs are available to draw
    DIRECT_MESSAGES_VIEW = "dms"
    SETTINGS_VIEW = "settings"
    SERVERS_VIEW = "server"

    def __init__(self, discord_client, main_event_loop):
        self.client = discord_client
        self.main_event_loop = main_event_loop
        self.message_being_edit = None
        self.viewing_server = True
        # When in edit mode, user prob doesn't want to lose what they were typing if pushing up was an accident
        self.string_input_before_message = ""
        # Where in the string the cursor's logically (not visually) at
        self.input_pos_x = 0
        # For whether the user has the modal open or not
        self.modal_open = False
        self.modal = None

        self.screen = curses.initscr()
        curses.cbreak()
        curses.start_color()
        self.screen.keypad(1)
        self.window_height, self.window_width = self.screen.getmaxyx()

        self._set_self_fields()

    def _set_self_fields(self):
        self.chan_center_width = self.window_width - config.CHAN_LIST_WIDTH - \
            config.USER_LIST_WIDTH - 4  # 4 vertical lines
        self.dm_center_width = self.window_width - \
            config.USER_LIST_WIDTH - 3  # 3 vertical lines
        # 9 = 1 for bottom help + 5 for top banner/channel + 3 for message box area
        self.chan_messages_height = self.window_height - 9
        # 3 = 1 for top, 1 for bottom help, 1 for bottom of ui
        self.chan_list_height = self.window_height - 3
        self.user_list_height = self.window_height - 3

        # Modal vars
        self.modal_width = (self.window_width // 4) * \
            2  # This keeps it an even number
        self.modal_center = self.modal_width // 2
        self.modal_button_width = 10  # Longest button: [ Cancel ]
        self.modal_single_button_space = self.modal_center - \
            (self.modal_button_width // 2) - 1
        self.modal_double_button_space = self.modal_center - \
            self.modal_button_width - 2

        # All the renderings
        self.rendered_lines = {
            'channel_top_bar': f'{UI.icons["top_left_corner"]}{UI.icons["horizontal"] * config.CHAN_LIST_WIDTH}{UI.icons["top_intersection"]}{UI.icons["horizontal"] * self.chan_center_width}{UI.icons["top_intersection"]}{UI.icons["horizontal"] * config.USER_LIST_WIDTH}{UI.icons["top_right_corner"]}',
            'channel_middle': f'{UI.icons["vertical"]}{" " * config.CHAN_LIST_WIDTH}{UI.icons["vertical"]}{" " * self.chan_center_width}{UI.icons["vertical"]}{" " * config.USER_LIST_WIDTH}{UI.icons["vertical"]}',
            'channel_middle_separator': f'{UI.icons["vertical"]}{" " * config.CHAN_LIST_WIDTH}{UI.icons["left_intersection"]}{UI.icons["horizontal"] * self.chan_center_width}{UI.icons["right_intersection"]}{" " * config.USER_LIST_WIDTH}{UI.icons["vertical"]}',
            'channel_bottom_bar': f'{UI.icons["bottom_left_corner"]}{UI.icons["horizontal"] * config.CHAN_LIST_WIDTH}{UI.icons["bottom_intersection"]}{UI.icons["horizontal"] * self.chan_center_width}{UI.icons["bottom_intersection"]}{UI.icons["horizontal"] * config.USER_LIST_WIDTH}{UI.icons["bottom_right_corner"]}',
            'dm_top_bar': f'{UI.icons["top_left_corner"]}{UI.icons["vertical"] * config.CHAN_LIST_WIDTH}{UI.icons["top_intersection"]}{UI.icons["vertical"] * self.dm_center_width}{UI.icons["top_right_corner"]}',
            'help': f'| /chans - Channels | /servers - Servers | /users - Users | /help - Help Menu | /exit - Exit |',
            'modal_top_bar': f'{UI.icons["top_left_corner"]}{UI.icons["horizontal"] * (self.modal_width - 2)}{UI.icons["top_right_corner"]}',
            'modal_middle': f'{UI.icons["vertical"]}{" " * (self.modal_width - 2)}{UI.icons["vertical"]}',
            'modal_single_button_top': f'{UI.icons["vertical"]}{" " * self.modal_single_button_space}{UI.icons["top_left_corner"]}{UI.icons["horizontal"] * (self.modal_button_width - 2)}{UI.icons["top_right_corner"]}{" " * self.modal_single_button_space}{UI.icons["vertical"]}',
            'modal_double_button_top': f'{UI.icons["vertical"]}{" " * self.modal_double_button_space}{UI.icons["top_left_corner"]}{UI.icons["horizontal"] * (self.modal_button_width - 2)}{UI.icons["top_right_corner"]}  {UI.icons["top_left_corner"]}{UI.icons["horizontal"] * (self.modal_button_width - 2)}{UI.icons["top_right_corner"]}{" " * self.modal_double_button_space}{UI.icons["vertical"]}',
            'modal_single_button_middle': f'{UI.icons["vertical"]}{" " * self.modal_single_button_space}{UI.icons["vertical"]}{" " * (self.modal_button_width - 2)}{UI.icons["vertical"]}{" " * self.modal_single_button_space}{UI.icons["vertical"]}',
            'modal_double_button_middle': f'{UI.icons["vertical"]}{" " * self.modal_double_button_space}{UI.icons["vertical"]}{" " * (self.modal_button_width - 2)}{UI.icons["vertical"]}  {UI.icons["vertical"]}{" " * (self.modal_button_width - 2)}{UI.icons["vertical"]}{" " * self.modal_double_button_space}{UI.icons["vertical"]}',
            'modal_single_button_bottom': f'{UI.icons["vertical"]}{" " * self.modal_single_button_space}{UI.icons["bottom_left_corner"]}{UI.icons["horizontal"] * (self.modal_button_width - 2)}{UI.icons["bottom_right_corner"]}{" " * self.modal_single_button_space}{UI.icons["vertical"]}',
            'modal_double_button_bottom': f'{UI.icons["vertical"]}{" " * self.modal_double_button_space}{UI.icons["bottom_left_corner"]}{UI.icons["horizontal"] * (self.modal_button_width - 2)}{UI.icons["bottom_right_corner"]}  {UI.icons["bottom_left_corner"]}{UI.icons["horizontal"] * (self.modal_button_width - 2)}{UI.icons["bottom_right_corner"]}{" " * self.modal_double_button_space}{UI.icons["vertical"]}',
            'modal_bottom': f'{UI.icons["bottom_left_corner"]}{UI.icons["horizontal"] * (self.modal_width - 2)}{UI.icons["bottom_right_corner"]}',
        }

        self.ui_locations = {
            "channels": {'x': 1, 'y': 1},
            # channel_chat of y=5: 2 for top banner/channel + 3 for separators
            "channel_chat": {'x': config.CHAN_LIST_WIDTH + 3, 'y': 5},
            "server_info": {'x': config.CHAN_LIST_WIDTH + 3, 'y': 1},
            "current_view_info": {'x': config.CHAN_LIST_WIDTH + 2, 'y': 3},
            "users": {'x': self.window_width - config.USER_LIST_WIDTH - 1, 'y': 1},
            "message_box": {'x': config.CHAN_LIST_WIDTH + 3, 'y': self.window_height - 3},
            # Right above the message_box
            "edit_note": {'x': config.CHAN_LIST_WIDTH + 3, 'y': self.window_height - 4},
            "modal": {'x': (self.window_width // 2) - (self.window_width // 4), 'y': (self.window_height // 2) - (self.window_height // 4)},
        }

        # space at start and end, so minus 2
        self.message_box_width = self.chan_center_width - 2
        self.message_box_center = (
            self.ui_locations['message_box']['x'] + (self.message_box_width // 2))

    async def _draw_channel_list(self):
        pass

    async def _draw_friends_list(self):
        pass

    def _clear_edit_mode(self) -> None:
        sc_addstr(self.screen, UI.icons['horizontal'] * len("EDIT MODE"),
                  self.ui_locations['edit_note']['y'], self.ui_locations['edit_note']['x'])
        self._clear_message_content()
        self.write_message_content(self.input_string)
        self.message_being_edit = None

    def _clear_message_content(self) -> None:
        sc_addstr(self.screen, " " * (self.chan_center_width - 1),
                  self.ui_locations['message_box']['y'], self.ui_locations['message_box']['x'])
        sc_move(self.screen, self.ui_locations['message_box']
                ['y'], self.ui_locations['message_box']['x'])

    def _draw_channel_window(self):
        screen = self.screen
        screen.clear()
        sc_move(screen, 0, 0)

        # draw each of the sections
        sc_addstr(screen, self.rendered_lines['channel_top_bar'])
        sc_addstr(screen, self.rendered_lines['channel_middle'])
        sc_addstr(screen, self.rendered_lines['channel_middle_separator'])
        sc_addstr(screen, self.rendered_lines['channel_middle'])
        sc_addstr(screen, self.rendered_lines['channel_middle_separator'])
        sc_addstr(
            screen, self.rendered_lines['channel_middle'] * self.chan_messages_height)
        sc_addstr(screen, self.rendered_lines['channel_middle_separator'])
        sc_addstr(screen, self.rendered_lines['channel_middle'])
        sc_addstr(screen, self.rendered_lines['channel_bottom_bar'])
        # Only draw the help line if the screen's wide enough
        if self.window_width >= len(self.rendered_lines['help']):
            sc_addstr(screen, self.rendered_lines['help'])

        sc_move(screen,
                self.ui_locations['message_box']['y'],
                self.ui_locations['message_box']['x'])

        sc_refresh(screen)

    def _handle_backspace(self, y: int, x: int) -> None:
        # Don't let backspace go before position 0 or attempt to erase something that doesn't exist
        if len(self.input_string) > 0 and self.input_pos_x > 0 and x >= self.ui_locations['message_box']['x']:
            # Moving back one index since we backspaced the previous character
            self.input_pos_x -= 1
            # Re-write the full input_string by removing the character backspaced
            self.input_string = self.input_string[:self.input_pos_x] + \
                self.input_string[self.input_pos_x+1:]
            # If the input_pos_x is at an index beyond the message_box's width, increment x so it doesn't
            # fall backwards twice. Also gotta handle the offset if the cursor's not at the end of the string
            if (x - self.ui_locations['message_box']['x'] > self.message_box_width or
                    self.input_pos_x + (len(self.input_string) - self.input_pos_x) >= self.message_box_width) and \
                    (len(self.input_string) > self.message_box_width and self.input_pos_x - (x - self.ui_locations['message_box']['x']) > 0):
                x += 1
            # Else, there's an offset and the cursor's at the end of the message_box, so handle it appropriately
            elif len(self.input_string) == self.message_box_width:
                x = self.input_pos_x + \
                    self.ui_locations['message_box']['x']
            # Move before writing
            sc_move(self.screen, y, x)
            self.write_message_content(
                display_string=self.input_string, keep_new_position=False,
                push_dir=1 if len(self.input_string) > self.message_box_width else 0)
        else:
            # Nothing got backspaced, move x back to where it was (curses moves it back automatically)
            sc_move(self.screen, y, x+1)

    def _handle_user_key(self, key: int) -> bool:
        """
        Handles the user key that was pushed

        Params:
          key: The key integer from curses.getch
        Returns:
          bool: Whether ENTER was pressed (process the input)
        """
        y, x = sc_getyx(self.screen)

        if self.modal_open:
            if key in [curses.KEY_UP, curses.KEY_DOWN, curses.KEY_LEFT, curses.KEY_RIGHT, curses.KEY_ENTER, 10, 13]:
                self.modal_open = self.modal.handle_input(key)
                if not self.modal_open:
                    # TODO: When navigation's a thing, be sure the _current_ channel/dm window/etc. is shown
                    if self.current_view == UI.SERVERS_VIEW:
                        asyncio.run_coroutine_threadsafe(
                            self.run_full_render(), self.main_event_loop)
                        self._move_to_message_box()
                    elif self.current_view == UI.DIRECT_MESSAGES_VIEW:
                        asyncio.run_coroutine_threadsafe(
                            self.go_to_dms(), self.main_event_loop)
                        self._move_to_message_box()
                    # TODO: else: settings
            return False
        elif self.navigating_channels:
            if key in [ascii.ESC, curses.KEY_UP, curses.KEY_DOWN, curses.KEY_ENTER, 10, 13]:
                self.navigating_channels = self.channel_list.handle_input(key)
                if not self.navigating_channels:
                    self._move_to_message_box()
            return False
        elif self.navigating_users:
            if key in [ascii.ESC, curses.KEY_UP, curses.KEY_DOWN, curses.KEY_ENTER, 10, 13]:
                self.navigating_users = self.user_list.handle_input(key)
                if not self.navigating_users:
                    self._move_to_message_box()
            return False

        if key == curses.KEY_ENTER or key == 10 or key == 13:
            self.input_pos_x = 0
            return True
        elif key == curses.KEY_BACKSPACE:
            self._handle_backspace(y, x)
            return False
        elif key == curses.KEY_LEFT:
            if x > self.ui_locations['message_box']['x'] and\
                    x - self.input_pos_x == self.ui_locations['message_box']['x'] or\
                    x - self.message_box_center > 0:
                sc_move(self.screen, y, x-1)
            else:
                self.write_message_content(self.input_string, False)
            if self.input_pos_x > 0:
                self.input_pos_x -= 1
            return False
        elif key == curses.KEY_RIGHT:
            end_of_message_box = self.ui_locations['message_box']['x'] + \
                self.message_box_width
            if x < end_of_message_box and\
                    self.input_pos_x < len(self.input_string) and\
                    (self.message_box_center + 1 - x > 0 or end_of_message_box >= len(self.input_string) - self.input_pos_x + x):
                sc_move(self.screen, y, x+1)
            elif self.input_pos_x < len(self.input_string):
                self.write_message_content(self.input_string, False)

            if self.input_pos_x < len(self.input_string):
                self.input_pos_x += 1
            return False
        elif key == curses.KEY_UP:
            asyncio.run_coroutine_threadsafe(
                edit_previous_message(self, self.client.user.id), self.main_event_loop)
            return False
        elif key == curses.KEY_DOWN:
            if self.message_being_edit != None:
                self.input_string = self.string_input_before_message
                self.input_pos_x = len(self.input_string)
                self._clear_edit_mode()
            return False
        elif key == curses.KEY_HOME:
            return False
        elif key == curses.KEY_END:
            return False
        elif key == curses.KEY_PPAGE:  # page up
            return False
        elif key == curses.KEY_NPAGE:  # page down
            return False
        elif key == ascii.ESC:
            return False
        elif key == ord('\t'):  # Tab key
            return False

        self.input_string = self.input_string[:self.input_pos_x] + \
            chr(key) + self.input_string[self.input_pos_x:]

        self.input_pos_x += 1

        # This has to be done each time simply because the user can type in the middle of the string, too.
        self.write_message_content(display_string=self.input_string,
                                   push_dir=1 if len(self.input_string) > self.input_pos_x else 0)
        sc_move(self.screen,
                self.ui_locations['message_box']['y'],
                min(x, self.ui_locations['message_box']['x'] + self.message_box_width))
        return False

    def _move_to_message_box(self):
        # Set the cursor to the message window
        sc_move(self.screen,
                self.ui_locations['message_box']['y'],
                self.ui_locations['message_box']['x'])

    async def draw_ui(self):
        """
        Draw the main UI at the start of the app
        """
        self._draw_channel_window()

    async def go_to_dms(self) -> None:
        """
        Transition the user to the Direct Messages UI
        """
        self.current_view = UI.DIRECT_MESSAGES_VIEW
        pass

    async def run_full_render(self) -> None:
        await render_current_server(self.screen, self.server,
                                    self.current_channel, self)
        self.channel_list.render_channels()
        self.user_list.render_users()
        # await self.messages_ui.render_messages(curses_screen, channel, ui)
        await render_messages(self.screen, self.current_channel, self)
        sc_refresh(self.screen)
        self._move_to_message_box()

    async def set_server(self, server: object) -> None:
        """
        Change the entire UI to move the user to a different server. Sets the current_channel
        to the first available one in the server.
        """
        # For other navigation lists
        self.channel_list = ChannelList(self)
        self.user_list = UserList(self)
        self.navigating_channels = False
        self.navigating_users = False

        self.current_view = UI.SERVERS_VIEW
        self.server = server
        self.channel_list.set_channels_info(server)

        # Sets the current channel to the one at the very top of the list
        self.current_channel = self.channel_list.current_channels[0]

        self.user_list.set_users_info()
        await self.run_full_render()

    async def update_messages(self, channel_id: int) -> None:
        """
        When a new message comes in, this method simply refreshes the message area.

        Params:
          channel: So we can determine whether we care or not to refresh the message feed
        """
        if channel_id == self.current_channel.id:
            await render_messages(self.screen, self.current_channel, self)
            if self.modal_open:
                self.modal.draw_modal()
            sc_refresh(self.screen)

    def get_user_input(self):
        """
        Listens for keyboard input and only ends when the user types /exit

        Raises:
          KeyboardInterrupt: When the user types /exit
        """
        while True:
            self.input_string = ""
            self.displayed_input_string = ""

            while True:
                key = self.screen.getch()
                end_loop = self._handle_user_key(key)
                if end_loop:
                    break

            # Clear out message bar
            self._clear_message_content()

            if self.message_being_edit:
                if self.input_string == "":
                    asyncio.run_coroutine_threadsafe(
                        self.message_being_edit.delete(), self.main_event_loop)
                else:
                    asyncio.run_coroutine_threadsafe(
                        self.message_being_edit.edit(content=self.input_string), self.main_event_loop)
                self.string_input_before_message = ""
                self.input_string = ""
                self._clear_edit_mode()
                self.message_being_edit = None
                continue

            if self.input_string.lower() == "/exit":
                curses.nocbreak()
                self.screen.keypad(0)
                curses.endwin()
                raise KeyboardInterrupt("/exit was typed")
            elif self.input_string.lower() == "/chans":
                self.channel_list.focus_on_list()
            elif self.input_string.lower() == "/servers":
                show_servers_list(self)
            elif self.input_string.lower() == "/users":
                self.user_list.focus_on_list()
            elif self.input_string.lower() == "/help":
                show_help_modal(self)
            elif self.input_string.lower().startswith("/shrug "):
                asyncio.run_coroutine_threadsafe(
                    self.current_channel.send(self.input_string[len('/shrug '):] + " ¯\_(ツ)_/¯"), self.main_event_loop)
            elif self.input_string.lower() == "/tableflip":
                asyncio.run_coroutine_threadsafe(
                    self.current_channel.send("(╯°□°）╯︵ ┻━┻"), self.main_event_loop)
            elif self.input_string.lower() == "/unflip":
                asyncio.run_coroutine_threadsafe(
                    self.current_channel.send("┬─┬ ノ( ゜-゜ノ)"), self.main_event_loop)
            elif self.input_string.lower().startswith("/nick"):
                asyncio.run_coroutine_threadsafe(
                    user_commands.change_user_name(self, self.input_string), self.main_event_loop)
            elif "@" in self.input_string:
                asyncio.run_coroutine_threadsafe(
                    user_commands.send_message_with_mentions(self, self.input_string), self.main_event_loop)
            else:
                asyncio.run_coroutine_threadsafe(self.current_channel.send(
                    self.input_string), self.main_event_loop)

    def write_message_content(self, display_string: str, keep_new_position: bool = True, push_dir: int = 0) -> None:
        """
        Shows the message in the message box. Is limited to the width of the message box and displays what it can up
        to the point of the cursor (and after if it's not at the end)

        Params:
          display_string: The string to display in the message box
          keep_new_position: Whether the cursor should "keep" the new position after it's typed the display string
          push_dir: A variable to help push a string that's longer than the message_box a direction a given amount
        """
        if len(display_string) > self.message_box_width:
            _, x = sc_getyx(self.screen)
            start = max(self.input_pos_x - abs(x - 1 - (-push_dir) - self.message_box_center) -
                        (self.message_box_center - self.ui_locations['message_box']['x']), 0)
            end = min(len(display_string), self.message_box_width + start)
            display_string = display_string[start:end]

        sc_addstr(self.screen, " " * (self.chan_center_width - 1),
                  self.ui_locations['message_box']['y'], self.ui_locations['message_box']['x'])
        new_y, new_x = sc_addstr(self.screen, display_string,
                                 self.ui_locations['message_box']['y'], self.ui_locations['message_box']['x'])
        if keep_new_position:
            sc_move(self.screen, new_y, new_x)

    def update_users_list(self) -> None:
        """
        Updates the UI for the users section
        """
        self.user_list.render_users()
        self.user_list.set_users_info()
        if self.modal_open:
            self.modal.draw_modal()
        sc_refresh(self.screen)

    def update_channels_list(self) -> None:
        """
        Updates the UI for the channels section
        """
        self.channel_list.render_channels()
        self.channel_list.set_channels_info(self.server)
        if self.modal_open:
            self.modal.draw_modal()
        sc_refresh(self.screen)
