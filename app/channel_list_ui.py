import asyncio
import curses
from curses import ascii
from tools import logger

from discord.guild import Guild
from discord.channel import TextChannel, VoiceChannel

from app import server_ui
from config import config
from tools.screen_funcs import sc_addstr, sc_refresh


class ChannelList:
    def __init__(self, ui: object):
        self.ui = ui
        self.selected_channel = -1
        self.channel_count = 0
        self.channel_scroll_offset = 0
        self.channel_last_ypos = 0

    def focus_on_list(self):
        self.ui.navigating_channels = True
        self.selected_channel = self.current_channels.index(
            self.ui.current_channel)
        curses.noecho()
        curses.curs_set(0)
        self.render_channels()
        sc_refresh(self.ui.screen)

    def handle_input(self, key: int) -> bool:
        if key == ascii.ESC:
            self.unfocus_on_list()
            return False
        elif key == curses.KEY_UP:
            self.selected_channel = max(0, self.selected_channel - 1)
            if self.selected_channel >= 0 and \
                    self.channel_last_ypos == self.ui.ui_locations['channels']['y'] + 2:
                self.channel_scroll_offset = max(
                    0, self.channel_scroll_offset - 1)
                # Top item will always be a category
                if self.channel_scroll_offset == 1:
                    self.channel_scroll_offset = 0
            self.render_channels()
            sc_refresh(self.ui.screen)
            return True
        elif key == curses.KEY_DOWN:
            self.selected_channel = min(
                self.channel_count - 1, self.selected_channel + 1)
            if self.selected_channel < self.channel_count - 1 and \
                    self.channel_last_ypos >= self.ui.ui_locations['channels']['y'] + self.ui.chan_list_height - 3:
                self.channel_scroll_offset += 1
            self.render_channels()
            sc_refresh(self.ui.screen)
            return True
        elif key == curses.KEY_ENTER or key == 10 or key == 13:
            self.ui.current_channel = self.current_channels[self.selected_channel]
            asyncio.run_coroutine_threadsafe(
                self.ui.run_full_render(), self.ui.main_event_loop)
            self.unfocus_on_list()
            return False
        return True

    def render_channels(self) -> None:
        """
        Renders all the channels list and categories on the channels list section
        """
        server = self.ui.server

        # Clear out current channel UI
        for i in range(self.ui.ui_locations['channels']['y'], self.ui.window_height - 2):
            sc_addstr(self.ui.screen, " " * config.CHAN_LIST_WIDTH,
                      i, self.ui.ui_locations['channels']['x'])

        categories = sorted(server.categories,
                            key=lambda category: category.position)
        y_position = 0
        channel_index = -1
        index = -1
        # This is to help w/ the scrolling up/down in the channels list for if the 2nd to last item is a category when
        # the starting index is zero
        last_pos_category = False

        def min_count_met() -> bool:
            """
            Draws '△'s at the top of the channels list and returns True if no more can be drawn
            """
            if self.channel_scroll_offset > 0 and y_position == 0:
                sc_addstr(
                    self.ui.screen,
                    f'   {"△" * (config.CHAN_LIST_WIDTH - 6)}   ',
                    self.ui.ui_locations['channels']['y'] + y_position,
                    self.ui.ui_locations['channels']['x'])
                return True
            return False

        def max_count_met() -> bool:
            """
            Draws '▽'s at the bottom of the channels list and returns True if no more can be drawn
            """
            if y_position >= self.ui.chan_list_height:
                sc_addstr(
                    self.ui.screen,
                    f'   {"▽" * (config.CHAN_LIST_WIDTH - 6)}   ',
                    y_position,
                    self.ui.ui_locations['channels']['x'])
                return True
            return False

        # Need this for converting to a member instead of user for permissions and such
        this_member = server.get_member(self.ui.client.user.id)
        for category in categories:
            category_permissions = category.permissions_for(this_member)
            if not category_permissions.read_messages:
                continue
            index += 1
            last_pos_category = True

            if max_count_met():
                return

            if index >= self.channel_scroll_offset:
                if not min_count_met():
                    category_name = f'▹{category.name}'
                    if len(category_name) > config.CHAN_LIST_WIDTH:
                        category_name = f'{category_name[:config.CHAN_LIST_WIDTH-4]}...'
                    sc_addstr(
                        self.ui.screen,
                        category_name,
                        self.ui.ui_locations['channels']['y'] + y_position,
                        self.ui.ui_locations['channels']['x'])
                y_position += 1

            channels = sorted(category.channels,
                              key=lambda channel: channel.position)
            channels = sorted(channels, key=lambda channel: channel.type)
            for channel in channels:
                channel_permissions = channel.permissions_for(this_member)
                if not channel_permissions.read_messages:
                    continue

                if max_count_met():
                    return

                index += 1
                channel_index += 1
                if index >= self.channel_scroll_offset:
                    if not min_count_met():
                        channel_name = channel.name
                        if type(channel) == TextChannel:
                            icon = ""
                            if channel.is_news():
                                icon = "📰"
                            elif channel.is_nsfw():
                                icon = "🔞"
                            private_channel = server_ui.everyone_role in channel.overwrites and channel.overwrites[server_ui.everyone_role].pair()[
                                1].read_messages
                            channel_name = f' {"🔒" if private_channel else "#"}{icon}{channel_name}'
                        elif type(channel) == VoiceChannel:
                            channel_name = f' 🔊{channel_name}'

                        if len(channel.name) > config.CHAN_LIST_WIDTH:
                            channel_name = f'{channel_name[:config.CHAN_LIST_WIDTH-4]}...'

                        sc_addstr(
                            self.ui.screen,
                            channel_name,
                            self.ui.ui_locations['channels']['y'] + y_position,
                            self.ui.ui_locations['channels']['x'],
                            server_ui.CUSTOM_COLOR_PAIRS['highlight'] if channel_index == self.selected_channel else 0)
                        # 2 = 1 for the down arrows + 1 for future index position
                        if last_pos_category and channel_index == self.selected_channel:
                            self.channel_last_ypos = y_position + 1
                        if channel_index - 2 == self.selected_channel:
                            self.channel_last_ypos = y_position
                    y_position += 1
                last_pos_category = False

    def set_channels_info(self, server: Guild) -> None:
        this_member = server.get_member(self.ui.client.user.id)

        # Gets a list of all sorted channels that are readable and stores it in memory
        self.current_channels = []
        categories = sorted([category for category in server.categories if category.permissions_for(
            this_member).read_messages], key=lambda category: category.position)
        for category in categories:
            channels = sorted([channel for channel in category.channels if channel.permissions_for(
                this_member).read_messages], key=lambda channel: channel.position)
            channels = sorted(channels, key=lambda channel: channel.type)
            self.current_channels.extend(channels)

        # Get the channel count for this server
        self.channel_count = len(self.current_channels)

    def unfocus_on_list(self) -> None:
        self.selected_channel = -1
        curses.echo()
        curses.curs_set(1)
        self.render_channels()
        sc_refresh(self.ui.screen)
