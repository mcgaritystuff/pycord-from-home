import discord
import re

from app import modal_ui
from tools import logger


async def send_message_with_mentions(ui: object, message_content: str) -> None:
    regex = re.compile(r'@([^#\s]+)(#[0-9]+)?')
    usernames = regex.findall(message_content)

    async def get_queried_id(name: str, discriminator: str, content: str) -> tuple:
        """
        Gets the queried member's id if only one exists.

        Returns:
            A tuple with (str, bool)
            str: the updated message_content with the mention. If str is empty, that means an error
                 occured. This function will show an error message to the UI.
            bool: If there was an error that got displayed
        """
        queried_members = await ui.server.query_members(query=name)
        errored = False
        if len(queried_members) == 1:
            return content.replace(f'@{name}{discriminator}', f'<@!{queried_members[0].id}>'), errored
        elif len(queried_members) > 2:
            modal_ui.show_error_message(
                ui, f'More than one username was found for the mention "{name}", so your message was not sent.')
            errored = True

        return None, errored

    for username in usernames:
        name = username[0]
        discriminator = username[1] if len(username) > 1 else ""

        if discriminator != "":
            respective_member = ui.server.get_member_named(
                f'{name}{discriminator}'
            )
            if respective_member != None:
                message_content = message_content.replace(
                    f'@{name}{discriminator}', f'<@!{respective_member.id}>')
            else:
                result, error = await get_queried_id(name, discriminator, message_content)
                if result != "" and result != None:
                    message_content = result
                elif error:
                    return
        else:
            result, error = await get_queried_id(name, discriminator, message_content)

            if result != "" and result != None:
                message_content = result
            elif error:
                return

    await ui.current_channel.send(message_content)


async def change_user_name(ui: object, message_content: str) -> None:
    member = await ui.server.fetch_member(ui.client.user.id)
    new_nickname = " ".join(message_content.split(
        " ")[1:])  # Remove `/nick` from command
    await member.edit(nick=new_nickname)
    await ui.update_messages(ui.current_channel.id)
