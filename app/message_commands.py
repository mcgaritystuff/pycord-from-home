from app import server_ui
from tools.screen_funcs import sc_addstr, sc_refresh


async def edit_previous_message(ui: object, client_user_id: int) -> None:
    """
    Sets the input string to the previous message. Stores the current message text to ui.string_input_before_message.

    Params:
      ui: The ui object to store the message_being_edit and ui.string_input_before_message to
      client_user_id: To make sure the user's trying to edit a message that belongs to them
    """
    # User's already editing previous message, don't do anything
    if ui.message_being_edit != None:
        return

    prev_message = None
    async for message in ui.current_channel.history():
        if message.author.id == client_user_id:
            prev_message = message
            break

    if prev_message != None:
        ui.string_input_before_message = ui.input_string
        ui.message_being_edit = message
        sc_addstr(ui.screen, "EDIT MODE",
                  ui.ui_locations['edit_note']['y'], ui.ui_locations['edit_note']['x'],
                  server_ui.CUSTOM_COLOR_PAIRS['edit_mode'])
        ui.input_string = prev_message.content
        ui.input_pos_x = len(prev_message.content)
        ui.write_message_content(prev_message.content)
        sc_refresh(ui.screen)
