import asyncio
import curses
from curses import ascii
from discord.enums import Status

from discord.guild import Guild
from discord.member import Member
from tools import logger

from app import server_ui
from config import config
from tools.screen_funcs import sc_addstr, sc_refresh


class UserList:
    def __init__(self, ui: object):
        self.ui = ui
        self.selected_user = -1
        self.user_count = 0
        self.user_scroll_offset = 0
        self.user_last_ypos = 0

    def focus_on_list(self):
        self.ui.navigating_users = True
        self.selected_user = 0
        curses.noecho()
        curses.curs_set(0)
        self.render_users()
        sc_refresh(self.ui.screen)

    def handle_input(self, key: int) -> bool:
        if key == ascii.ESC:
            self.unfocus_on_list()
            return False
        elif key == curses.KEY_UP:
            self.selected_user = max(0, self.selected_user - 1)
            if self.selected_user >= 0:
                if self.user_last_ypos == self.ui.ui_locations['users']['y'] + 2:
                    self.user_scroll_offset = max(
                        0, self.user_scroll_offset - 1)
                elif self.user_last_ypos <= self.ui.ui_locations['users']['y'] + 3:
                    self.user_scroll_offset = max(
                        0, self.user_scroll_offset - 2)

                # Top item will always be a role
                if self.user_scroll_offset == 1:
                    self.user_scroll_offset = 0
            self.render_users()
            sc_refresh(self.ui.screen)
            return True
        elif key == curses.KEY_DOWN:
            self.selected_user = min(
                self.user_count - 1, self.selected_user + 1)
            if self.selected_user < self.user_count - 1:
                if self.user_last_ypos == self.ui.ui_locations['users']['y'] + self.ui.user_list_height - 3:
                    self.user_scroll_offset += 1
                elif self.user_last_ypos >= self.ui.ui_locations['users']['y'] + self.ui.user_list_height - 5:
                    self.user_scroll_offset += 2
            self.user_scroll_offset = min((self.user_count - self.ui.user_list_height) + (
                self.role_count * 2) - 3, self.user_scroll_offset)

            self.render_users()
            sc_refresh(self.ui.screen)
            return True
        elif key == curses.KEY_ENTER or key == 10 or key == 13:
            # TODO - show modal w/ choices
            self.unfocus_on_list()
            return False
        return True

    def render_users(self) -> None:
        """
        Renders all the users and groups of the current channel

        Params:
        curses_screen: the screen initialized by curses
        channel: the current channel to show
        ui: the UI object calling this (so we can get locations, etc.)
        """
        # Clear out current users UI
        for i in range(self.ui.ui_locations['users']['y'], self.ui.window_height - 2):
            sc_addstr(self.ui.screen, " " * config.USER_LIST_WIDTH,
                      i, self.ui.ui_locations['users']['x'])

        # Get all the roles to properly show in the list in order, (highest->lowest order)
        roles = self.ui.current_channel.guild.roles[::-1]

        # Keep track of members already shown up somewhere on the list
        members_shown = []
        y_position = 0
        user_index = -1
        index = -1

        def min_count_met() -> bool:
            """
            Draws '△'s at the top of the users list and returns True if no more can be drawn
            """
            if self.user_scroll_offset > 0 and y_position == 0:
                sc_addstr(
                    self.ui.screen,
                    f'   {"△" * (config.USER_LIST_WIDTH - 6)}   ',
                    self.ui.ui_locations['users']['y'] + y_position,
                    self.ui.ui_locations['users']['x'])
                return True
            return False

        def max_count_met() -> bool:
            """
            Draws 'v's at the bottom of the users list and returns True if no more can be drawn
            """
            if y_position == self.ui.ui_locations['users']['y'] + self.ui.user_list_height - 1:
                sc_addstr(
                    self.ui.screen,
                    f'   {"▽" * (config.USER_LIST_WIDTH - 6)}   ',
                    y_position,
                    self.ui.ui_locations['users']['x'])
                return True
            return False

        def draw_member(member: Member, y_position: int) -> None:
            """
            Draws the given member's name on the list

            Params:
            member: the member to draw on the list
            y_position: the current position on the list
            """
            member_name = f' {"📱" if member.is_on_mobile() else server_ui.STATUS_ICONS[str(member.status)]} {member.display_name}#{member.discriminator}'
            if member == self.ui.current_channel.guild.owner:
                member_name += " 👑"
            elif member.bot:
                member_name += " 🤖"
            if len(member_name) > config.USER_LIST_WIDTH:
                member_name = f'{member_name[:config.USER_LIST_WIDTH-4]}...'
            sc_addstr(
                self.ui.screen,
                member_name,
                y_position + self.ui.ui_locations['users']['y'],
                self.ui.ui_locations['users']['x'],
                server_ui.CUSTOM_COLOR_PAIRS['highlight'] if user_index == self.selected_user else server_ui.role_colors[role.name]['color_pair'])
            # 2 = 1 for the down arrows + 1 for future index position
            if user_index == self.selected_user:
                self.user_last_ypos = y_position

        # Draw all the roles that are hoisted
        for role in roles:
            # if the role is not supposed to show separately, don't do it
            if not role.hoist and role.name != "everyone":
                continue
            index += 1

            if max_count_met():
                return

            members = list(filter(
                lambda member: role in member.roles and member not in members_shown and member.top_role == role and member.status != Status.offline and member.status != Status.invisible, self.ui.current_channel.members))
            # Don't draw roles w/ no members in them that are online
            if len(members) == 0:
                continue

            if index >= self.user_scroll_offset:
                # Write the role name to the screen
                if not min_count_met():
                    role_name = f'{role.name} - {len(members)}'.upper()
                    if len(role_name) > config.USER_LIST_WIDTH:
                        role_name = f'{role_name[:config.USER_LIST_WIDTH-4]}...'
                    sc_addstr(
                        self.ui.screen,
                        role_name,
                        y_position + self.ui.ui_locations['users']['y'],
                        self.ui.ui_locations['users']['x'])
                y_position += 1

            for member in members:
                if max_count_met():
                    return
                index += 1
                user_index += 1
                if index >= self.user_scroll_offset:
                    if not min_count_met():
                        draw_member(member, y_position)
                        members_shown.append(member)
                    y_position += 1
            if index >= self.user_scroll_offset:
                if max_count_met():
                    return
                y_position += 1

        # If there's still room, draw all members who are online and offline respectively

        online_members = list(filter(
            lambda member: member not in members_shown and member.status != Status.offline and member.status != Status.invisible and not member.top_role.hoist, self.current_users))
        offline_members = list(filter(
            lambda member: member not in members_shown and member not in online_members and (member.status == Status.offline or member.status == Status.invisible), self.current_users))

        if len(online_members) > 0:
            if not min_count_met():
                if max_count_met():
                    return
                sc_addstr(
                    self.ui.screen,
                    f'ONLINE - {len(online_members)}',
                    y_position + self.ui.ui_locations['users']['y'],
                    self.ui.ui_locations['users']['x'])
                y_position += 1
            for member in online_members:
                if max_count_met():
                    return
                index += 1
                user_index += 1
                if index >= self.user_scroll_offset:
                    if not min_count_met():
                        draw_member(member, y_position)
                        members_shown.append(member)
                    y_position += 1
            if index >= self.user_scroll_offset:
                if max_count_met():
                    return
                y_position += 1

        if len(offline_members) > 0:
            if not min_count_met():
                if max_count_met():
                    return
                sc_addstr(
                    self.ui.screen,
                    f'OFFLINE - {len(offline_members)}',
                    y_position + self.ui.ui_locations['users']['y'],
                    self.ui.ui_locations['users']['x'])
                y_position += 1
            for member in offline_members:
                if max_count_met():
                    return
                index += 1
                user_index += 1
                if index >= self.user_scroll_offset:
                    if not min_count_met():
                        draw_member(member, y_position)
                        members_shown.append(member)
                    y_position += 1

    def set_users_info(self) -> None:
        self.current_users = []
        roles = self.ui.current_channel.guild.roles[::-1]
        for role in roles:
            members = list(filter(
                lambda member: role in member.roles and member not in self.current_users, self.ui.current_channel.members))
            self.current_users.extend(members)

        # Get the user count for this server
        self.user_count = len(self.current_users)
        self.role_count = len([role for role in roles if role.hoist and len(
            [member for member in role.members if member.status != Status.offline and member.status != Status.invisible and member.top_role == role])])

    def unfocus_on_list(self) -> None:
        self.selected_user = -1
        curses.echo()
        curses.curs_set(1)
        self.render_users()
        sc_refresh(self.ui.screen)
