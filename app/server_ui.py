from discord.message import Message
from resources import emoji_dictionary
import curses
import emoji
import re

from datetime import datetime
from discord import Guild
from discord.abc import GuildChannel
from discord.channel import TextChannel, VoiceChannel
from discord.enums import Status
from discord.member import Member
from textwrap import wrap

from config import config
from tools import logger
from tools.screen_funcs import sc_addstr, sc_refresh

STATUS_ICONS = {
    "dnd": "🔴",
    "do_not_disturb": "🔴",
    "idle": "🟡",
    "invisible": "⚫",
    "offline": "⚫",
    "online": "🟢",
}

role_colors = {}

CUSTOM_COLOR_PAIRS = {
    "highlight": 1,
    "edit_mode": 2,
    "inverted": 3
}

# The everyone role. For keeping for things like permissions and stuff.
everyone_role = None

max_reply_length = 0

mention_separator = 'ᘸ'
emoji_buffer = 'ֆ'


async def render_current_server(curses_screen: object, server: Guild, channel: GuildChannel, ui: object) -> None:
    """
    Renders the server screen with the given channel

    Params:
      curses_screen: the screen initialized by curses
      server: the discord server to show
      channel: the current channel to show
      ui: the UI object calling this (so we can get locations, etc.)
    """
    # Draw the current server's information on the top
    server_name = f'Current Server: {server.name}{" - " + server.description if server.description != None else ""}'
    if len(server_name) > ui.chan_center_width:
        server_name = f'{server_name[:ui.chan_center_width-3]}...'
    sc_addstr(curses_screen, " " * (ui.chan_center_width - 1),
              ui.ui_locations['server_info']['y'], ui.ui_locations['server_info']['x'])
    sc_addstr(curses_screen, server_name,
              ui.ui_locations['server_info']['y'], ui.ui_locations['server_info']['x'])

    # Draw the current channel and topic
    channel_name = f' #{channel.name}{" - " + channel.topic if channel.topic != None else ""}'
    if len(channel_name) > ui.chan_center_width:
        channel_name = f'{channel_name[:ui.chan_center_width-3]}...'
    sc_addstr(curses_screen, " " * (ui.chan_center_width - 1),
              ui.ui_locations['current_view_info']['y'], ui.ui_locations['current_view_info']['x'])
    sc_addstr(curses_screen, channel_name,
              ui.ui_locations['current_view_info']['y'], ui.ui_locations['current_view_info']['x'])

    # Get all the roles and their colors
    global role_colors
    role_colors = {role.name: {'r': role.color.r, 'g': role.color.g,
                               'b': role.color.b} for role in server.roles}

    # Set other needed global vars
    global max_reply_length
    max_reply_length = ui.chan_center_width // 4

    index = 0
    for role in server.roles:
        current_color_num = curses.COLORS - index - 1
        role_colors[role.name]['color_pair'] = current_color_num
        # 255 => rgb is set for 0-255 on each color in Discord, but curses is 0-1000, so gotta convert it
        if role.name == '@everyone':
            # Make '@everyone' role white (Easier to see)
            global everyone_role
            role_colors[role.name]['r'], role_colors[role.name]['g'], role_colors[role.name]['b'] = 255, 255, 255
            everyone_role = role

        curses.init_color(current_color_num, int((role_colors[role.name]['r'] / 255)*1000),
                          int((role_colors[role.name]['g'] / 255)*1000), int((role_colors[role.name]['b'] / 255)*1000))
        curses.init_pair(current_color_num, current_color_num, 0)
        index += 1
        # Max out at 250 role colors
        if index > 250:
            break
    # Message highlight colors
    curses.init_color(1, 1000, 1000, 1000)  # White
    curses.init_color(2, 300, 300, 300)  # Gray-ish
    curses.init_color(3, 1000, 0, 0)  # Red
    curses.init_color(4, 0, 0, 0)  # Black
    curses.init_pair(CUSTOM_COLOR_PAIRS["highlight"], 1, 2)
    curses.init_pair(CUSTOM_COLOR_PAIRS["edit_mode"], 3, 4)
    curses.init_pair(CUSTOM_COLOR_PAIRS["inverted"], 4, 1)


def _set_mentions(message: Message, content: str, client_user: int) -> tuple:
    """
    Replaces mentions in a given text with usernames/channels

    Params:
      message: The discord message object
      content: The string to parse/update
      client_user: The person logged in

    Returns: 
      tuple: A tuple containing a (str,bool)
      str: The result of updating content
      bool: If the person logged in was mentioned or not
    """
    client_user_mentioned = False
    current_member = message.guild.get_member(client_user.id)
    # Replace mentions with usernames
    user_regex = re.compile(r'<@(!?)([0-9]+)>')
    channel_regex = re.compile(r'<#([0-9]+)>')
    role_regex = re.compile(r'<@&([0-9]+)>')
    for match in user_regex.finditer(content):
        try:
            member = next(filter(
                lambda member: member.id == int(
                    match.group(2)), message.mentions
            ))

            content = re.sub(
                match.group(), f'{mention_separator}@{member.display_name}{mention_separator}', content)
            if member.id == client_user.id:
                client_user_mentioned = True
        except:
            continue
    for match in channel_regex.finditer(content):
        try:
            channel = next(filter(
                lambda channel: channel.id == int(
                    match.group(1)), message.channel_mentions
            ))

            content = re.sub(
                match.group(), f'{mention_separator}#{channel.name}{mention_separator}', content)
        except:
            continue
    for match in role_regex.finditer(content):
        try:
            role = next(filter(
                lambda role: role.id == int(
                    match.group(1)), message.role_mentions
            ))

            content = re.sub(
                match.group(), f'{mention_separator}@{role.name}{mention_separator}', content)
            if role in current_member.roles:
                client_user_mentioned = True
        except:
            continue

    return content, client_user_mentioned


def _set_emojis(content: str) -> str:
    """
    Sets the emojis to text matching the :<content>: syntax

    Returns:
      content: The updated text with emojis
    """
    return re.sub(
        r'(:[^\s]+:)',
        lambda res: emoji_dictionary.emojis[res.group(0)] if res.group(
            0) in emoji_dictionary.emojis else res.group(0),
        content
    )


async def render_messages(curses_screen: object, channel: GuildChannel, ui: object) -> None:
    """
    Renders all the messages in the given channel

    Params:
      curses_screen: the screen initialized by curses
      channel: the current channel to show
      ui: the UI object calling this (so we can get drawn UI locations, etc.)    
    """
    # Clear out current message UI
    for i in range(0, ui.chan_messages_height):
        sc_addstr(curses_screen, " " * (ui.chan_center_width - 1),
                  ui.ui_locations['channel_chat']['y'] + i, ui.ui_locations['channel_chat']['x'])

    # Start from the bottom and move on up!
    y_position = ui.ui_locations['channel_chat']['y'] + \
        ui.chan_messages_height - 1
    # A boolean to break out of all loops
    out_of_room = False

    async for message in channel.history(limit=ui.chan_messages_height):
        if out_of_room:
            break

        reply = ""
        message_content = ""
        client_user_mentioned = False

        if message.reference != None:
            reference_message = None
            if message.reference.cached_message != None:
                reference_message = message.reference.cached_message
            else:
                reference_message = await channel.fetch_message(message.reference.message_id)
            content, client_user_mentioned = _set_mentions(
                reference_message, reference_message.content, ui.client.user)
            content = _set_emojis(content)
            reply = f'{content[:min(len(content), max_reply_length)] + ("..." if len(content) > max_reply_length else "")}\n'

        last_edited = message.edited_at

        if config.SHOW_MESSAGE_TIMESTAMPS:
            message_date = message.created_at
            format = config.NEW_MESSAGE_FORMAT if (
                datetime.now() - message_date).days <= 30 else config.OLD_MESSAGE_FORMAT
            message_content = f'{message.author.display_name} ({datetime.strftime(message_date, format)}){" [edited]" if last_edited != None else ""}{"↩️ " if reply != "" else ""}: {reply}{message.content}'
        else:
            message_content = f'{message.author.display_name} {" [edited]" if last_edited != None else ""}{"↩️ " if reply != None else ""}: {reply}{message.content}'

        # Replace mentions with usernames
        message_content, client_user_mentioned = _set_mentions(
            message, message_content, ui.client.user)

        # Split by all existing new lines before splitting by length
        lines = message_content.split('\n')[::-1]
        # Then for each of those lines, we'll try to print them out but wordwrap as needed (aka splitting by length)
        for line in lines:
            if out_of_room:
                break

            # Copy for reference later
            original_line = line
            line = _set_emojis(line)

            # This is a very hacky way to get emojis to keep lines breaking properly. Emojis take 2x the width
            # of normal characters, so this adds a very unlikely character to the mix to increase the length, which
            # allows the wrap() method to do its magic properly (in case emojis are all in one line without space).
            # Then within the `while len(split_line) > 0` loop, it starts off (After popping) with removing this extra
            # character.
            emoji_list = emoji.emoji_lis(line)
            index = 0
            for item in emoji_list:
                line = line[:item['location']-1+index] + \
                    emoji_buffer + line[item['location']+index-1:]
                index += 1
            split_line = wrap(line, width=ui.chan_center_width - 1)

            while len(split_line) > 0:
                curr_line = split_line.pop()
                curr_line = re.sub(emoji_buffer, '', curr_line)
                curr_y = y_position
                curr_x = ui.ui_locations['channel_chat']['x']

                # Last line, which means it's the line w/ the username to color
                if len(split_line) == 0 and not client_user_mentioned and lines[-1] == original_line:
                    username = curr_line.split()[0]
                    curr_line = " " + " ".join(curr_line.split()[1:])
                    curr_y, curr_x = sc_addstr(
                        curses_screen,
                        username,
                        curr_y,
                        curr_x,
                        role_colors[message.author.top_role.name if type(
                            message.author) == Member else '@everyone']['color_pair']
                    )
                if client_user_mentioned:
                    curr_line = curr_line.replace(mention_separator, '')
                    curr_y, curr_x = sc_addstr(
                        curses_screen, curr_line, curr_y, curr_x,
                        CUSTOM_COLOR_PAIRS['highlight'])
                else:
                    if "@" in curr_line or "#" in curr_line:
                        colored_line_split = re.split(
                            f"{mention_separator}([@#][^{mention_separator}]+){mention_separator}", curr_line)
                        for colored_line in colored_line_split:
                            if "@" in colored_line:
                                user = ui.server.get_member_named(
                                    colored_line[1:])
                                role = role_colors[colored_line[1:]
                                                   ] if colored_line[1:] in role_colors else None
                                if user != None:
                                    curr_y, curr_x = sc_addstr(
                                        curses_screen, colored_line, curr_y, curr_x, role_colors[user.top_role.name]['color_pair'])
                                elif role != None:
                                    curr_y, curr_x = sc_addstr(
                                        curses_screen, colored_line, curr_y, curr_x, role['color_pair'])
                                else:
                                    curr_y, curr_x = sc_addstr(
                                        curses_screen, colored_line, curr_y, curr_x)
                            elif "#" in colored_line:
                                channel_exists = len(
                                    [channel for channel in ui.channel_list.current_channels if channel.name == colored_line[1:]]) > 0
                                curr_y, curr_x = sc_addstr(curses_screen, colored_line, curr_y, curr_x,
                                                           CUSTOM_COLOR_PAIRS['highlight'] if channel_exists else 0)
                            else:
                                curr_y, curr_x = sc_addstr(
                                    curses_screen, colored_line, curr_y, curr_x)
                    else:
                        curr_y, curr_x = sc_addstr(
                            curses_screen, curr_line, curr_y, curr_x)

                if client_user_mentioned:
                    curr_y, curr_x = sc_addstr(curses_screen, " " * (
                        ui.ui_locations['users']['x'] - curr_x - 1), curr_y, curr_x, CUSTOM_COLOR_PAIRS['highlight'])
                y_position -= 1

                if y_position < ui.ui_locations['channel_chat']['y']:
                    out_of_room = True
                    break
