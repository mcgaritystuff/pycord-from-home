import curses
from textwrap import wrap
from typing import List

from app import server_ui
from tools import logger
from tools.screen_funcs import sc_addstr, sc_move


def show_error_message(ui: object, message: str) -> None:
    ui.modal = Modal(message.split("\n"), False, ui)
    ui.modal.draw_modal()


def show_help_modal(ui: object) -> None:
    help_text = \
        "General Commands:\n" +\
        "/chans     - Lets you navigate the channels in the current server (Press ESC to cancel)\n" +\
        "/servers   - Opens a modal so you can select a server you're a part of (or join a new one)\n" +\
        "/users     - Lets you navigate the users list in the current server or friend's list (Press ESC to cancel)\n" +\
        "/help      - Shows this help screen\n" +\
        "/exit      - Closes out of Pycord From Home\n" +\
        "\n" +\
        "Other:\n" +\
        "/shrug     - Inserts ¯\_(ツ)_/¯ at the end of your message\n" +\
        "/tableflip - On its own only, sends (╯°□°）╯︵ ┻━┻\n" +\
        "/unflip    - On its own only, sends ┬─┬ ノ( ゜-゜ノ)\n" +\
        "/nick      - Changes your server nickname to anything after the command"
    ui.modal = Modal(help_text.split("\n"), False, ui)
    ui.modal.draw_modal()


class Modal:
    UNSELECTED = -1
    OK = 0
    CANCEL = 1

    BASE_MODAL_HEIGHT = 8

    def __init__(self, text_lines: List[str], has_cancel: bool, ui: object):
        """
        Initializes a Modal window. The height of the content section will be dynamically sized.

        Params:
            text_lines: A word-wrapped list of text to show on the modal
            has_cancel: Whether or not to display the cancel button
            ui: The UI object handling positioning/drawing/etc.
        """
        self.current_modal_button_selected = Modal.OK
        self.has_cancel = has_cancel
        self.ui = ui
        self.scroll_offset = 0

        self.text_lines = []
        for line in text_lines:
            # 5 = 2 side lines + 2 padding + 1 scroll bar
            wrapped_lines = wrap(line, ui.modal_width - 5)
            for wrapped_line in wrapped_lines:
                self.text_lines.append(wrapped_line)

        self.modal_height = min(
            Modal.BASE_MODAL_HEIGHT + len(self.text_lines), ui.window_height // 2)
        self.text_content_height = self.modal_height - Modal.BASE_MODAL_HEIGHT
        # Define scrollbar variables if it needs to exist
        if self.text_content_height < len(self.text_lines):
            self.scroll_bar_height = max(1, int(
                self.text_content_height * (self.text_content_height / len(self.text_lines))))
            self.offsets_per_scroll = (
                len(self.text_lines) - self.text_content_height) / self.text_content_height
        else:
            self.offsets_per_scroll = 0

    def _draw_modal_window(self) -> None:
        curr_y = self.ui.ui_locations['modal']['y']
        curr_x = self.ui.ui_locations['modal']['x']
        ui = self.ui
        sc_move(ui.screen, curr_y, curr_x)

        def _draw_modal_line(line: str) -> int:
            sc_addstr(ui.screen, line, curr_y, curr_x)
            return curr_y+1

        curr_y = _draw_modal_line(ui.rendered_lines['modal_top_bar'])
        # 5 = top line + bottom line + 3 for buttons + 1 for gap at bottom of button
        for _ in range(0, self.modal_height - 6):
            curr_y = _draw_modal_line(
                ui.rendered_lines['modal_middle'])
        modal_type = 'double' if self.has_cancel else 'single'

        curr_y = _draw_modal_line(
            ui.rendered_lines[f'modal_{modal_type}_button_top'])
        curr_y = _draw_modal_line(
            ui.rendered_lines[f'modal_{modal_type}_button_middle'])
        curr_y = _draw_modal_line(
            ui.rendered_lines[f'modal_{modal_type}_button_bottom'])
        curr_y = _draw_modal_line(ui.rendered_lines['modal_middle'])
        curr_y = _draw_modal_line(ui.rendered_lines['modal_bottom'])

        if len(self.text_lines) > self.text_content_height:
            self._draw_scrollbar()

    def _draw_scrollbar(self) -> None:
        scroll_y = self.ui.ui_locations['modal']['y'] + 1
        scroll_x = self.ui.ui_locations['modal']['x'] + self.ui.modal_width - 2

        if self.scroll_offset == len(self.text_lines) - self.text_content_height:
            scrollbar_start = self.text_content_height - self.scroll_bar_height
        else:
            scrollbar_start = int((self.scroll_offset /
                                   len(self.text_lines)) * self.text_content_height)
        sc_addstr(self.ui.screen, "▵", scroll_y, scroll_x)
        scroll_y += 1

        for i in range(0, self.text_content_height):
            if i >= scrollbar_start and i < scrollbar_start + self.scroll_bar_height:
                sc_addstr(self.ui.screen, "█", scroll_y, scroll_x)
            scroll_y += 1

        sc_addstr(self.ui.screen, "▿", scroll_y, scroll_x)

    def _draw_modal_text(self) -> None:
        ui = self.ui
        button_y = ui.ui_locations['modal']['y'] + self.modal_height - 4
        padding = ui.modal_double_button_space if self.has_cancel else ui.modal_single_button_space
        x_1 = ui.ui_locations['modal']['x'] + padding + 5  # all space-related
        sc_addstr(
            ui.screen, "OK", button_y, x_1,
            server_ui.CUSTOM_COLOR_PAIRS['inverted'] if self.current_modal_button_selected == 0 else 0
        )

        if self.has_cancel:
            x_2 = x_1 + ui.modal_button_width
            sc_addstr(
                ui.screen, "Cancel", button_y, x_2,
                server_ui.CUSTOM_COLOR_PAIRS['inverted'] if self.current_modal_button_selected == 1 else 0
            )

        curr_y = self.ui.ui_locations['modal']['y'] + 2
        curr_x = self.ui.ui_locations['modal']['x'] + 2

        for i in range(self.scroll_offset, len(self.text_lines)):
            if curr_y >= self.text_content_height + self.ui.ui_locations['modal']['y'] + 2:
                break
            sc_addstr(self.ui.screen, self.text_lines[i], curr_y, curr_x)
            curr_y += 1

    def _draw_modal_ui(self) -> None:
        self._draw_modal_window()
        self._draw_modal_text()

    def draw_modal(self) -> None:
        """
        Draws the modal using the ui's defined ui_location for 'modal' and 'modal_width'/'modal_heigh'

        Params:
            ui: The ui that defines the modal position and tracks the modal
            text_lines: The list of text lines to draw into the modal
            has_cancel: Whether to show the cancel button (defaults True) - Note [OK] is always drawn
        """
        self.ui.modal_open = True
        curses.noecho()
        curses.curs_set(0)
        self._draw_modal_ui()

    def handle_input(self, key: int) -> bool:
        """
        Handles the input from the user

        Params:
            key: The key that was pushed

        Returns:
            bool: True if the modal is still open, False otherwise
        """
        if key == curses.KEY_LEFT and self.current_modal_button_selected == Modal.CANCEL:
            self.current_modal_button_selected = Modal.OK
            self._draw_modal_ui()
            return True
        elif key == curses.KEY_RIGHT and self.current_modal_button_selected == Modal.OK and self.has_cancel:
            self.current_modal_button_selected = Modal.CANCEL
            self._draw_modal_ui()
            return True
        elif key == curses.KEY_UP:
            self.scroll_offset = max(0, self.scroll_offset - 1)
            self._draw_modal_ui()
            return True
        elif key == curses.KEY_DOWN:
            self.scroll_offset = min(
                len(self.text_lines) - self.text_content_height, self.scroll_offset + 1)
            self._draw_modal_ui()
            return True
        elif key == curses.KEY_ENTER or key == 10 or key == 13:
            if self.current_modal_button_selected == Modal.CANCEL:
                curses.echo()
                curses.curs_set(1)
                return False
            # TODO: Implement something to happen on 'okay' (for now just does the same as cancel)
            else:
                curses.echo()
                curses.curs_set(1)
                return False

        return True
