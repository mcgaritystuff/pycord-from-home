import asyncio
from app import server_ui
import curses
from tools.screen_funcs import sc_addstr
from typing import List

from app.modal_ui import Modal
from tools import logger


def show_servers_list(ui: object) -> None:
    text = "\n".join([guild.name for guild in ui.client.guilds])
    ui.modal = ServerModal(text.split("\n"), True, ui)
    ui.modal.draw_modal()


class ServerModal(Modal):
    def __init__(self, text_lines: List[str], has_cancel: bool, ui: object):
        super().__init__(text_lines, has_cancel, ui)
        self.servers_list = ui.client.guilds
        self.current_server_index = self.servers_list.index(ui.server)
        self.current_modal_button_selected = Modal.UNSELECTED

        if self.current_server_index > self.text_content_height - self.scroll_offset - 1:
            self.scroll_offset = min(
                len(self.text_lines) - self.text_content_height, self.text_content_height - self.scroll_offset)

    def _draw_modal_text(self) -> None:
        super()._draw_modal_text()
        y_position = self.ui.ui_locations['modal']['y'] + 2 + \
            (self.current_server_index - self.scroll_offset)
        x_position = self.ui.ui_locations['modal']['x'] + 2
        sc_addstr(
            self.ui.screen,
            self.servers_list[self.current_server_index].name,
            y_position, x_position,
            server_ui.CUSTOM_COLOR_PAIRS['inverted'])

    def handle_input(self, key: int) -> bool:
        """
        Handles the input from the user

        Params:
            key: The key that was pushed

        Returns:
            bool: True if the modal is still open, False otherwise
        """
        if key == curses.KEY_LEFT and self.current_modal_button_selected == Modal.CANCEL:
            self.current_modal_button_selected = Modal.OK
            self._draw_modal_ui()
            return True
        elif key == curses.KEY_RIGHT and self.current_modal_button_selected == Modal.OK and self.has_cancel:
            self.current_modal_button_selected = Modal.CANCEL
            self._draw_modal_ui()
            return True
        elif key == curses.KEY_RIGHT and self.current_modal_button_selected == Modal.UNSELECTED:
            self.current_modal_button_selected = Modal.OK
            self._draw_modal_ui()
            return True
        elif key == curses.KEY_UP:
            if self.current_modal_button_selected != Modal.UNSELECTED:
                self.current_modal_button_selected = Modal.UNSELECTED
            else:
                self.current_server_index = max(
                    0, self.current_server_index - 1)
                if self.current_server_index < self.scroll_offset:
                    self.scroll_offset = max(0, self.scroll_offset - 1)
            self._draw_modal_ui()
            return True
        elif key == curses.KEY_DOWN:
            outside_range = True if self.current_server_index + \
                1 == len(self.servers_list) else False
            self.current_server_index = min(
                len(self.servers_list) - 1, self.current_server_index + 1)

            if outside_range:
                if self.current_modal_button_selected == Modal.UNSELECTED:
                    self.current_modal_button_selected = Modal.OK
            elif self.current_server_index > self.text_content_height - self.scroll_offset - 1:
                self.scroll_offset = min(
                    len(self.text_lines) - self.text_content_height, self.scroll_offset + 1)

            self._draw_modal_ui()
            return True
        elif key == curses.KEY_ENTER or key == 10 or key == 13:
            if self.current_modal_button_selected == Modal.CANCEL:
                curses.echo()
                curses.curs_set(1)
                return False
            else:  # Clicked OK (or just pressed enter on the server selected)
                curses.echo()
                curses.curs_set(1)
                asyncio.run_coroutine_threadsafe(self.ui.set_server(
                    self.servers_list[self.current_server_index]), self.ui.main_event_loop)
                return False

        return True
