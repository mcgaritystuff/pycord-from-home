import asyncio
from asyncio.events import AbstractEventLoop
import discord
import traceback
import sys

from threading import Thread

from app.ui import UI
from tools import logger


class PycordFromHome(discord.Client):
    async def on_ready(self):
        main_loop = asyncio.get_event_loop()
        self.ui = UI(self, main_loop)
        await self.ui.draw_ui()
        # Get the initial server to start in. Else, set in the DM screen
        if len(self.guilds) > 0:
            await self.ui.set_server(self.guilds[0])
        else:
            await self.ui.go_to_dms()

        def start_thread(loop: AbstractEventLoop) -> None:
            asyncio.set_event_loop(loop)
            try:
                loop.run_forever()
            except KeyboardInterrupt:
                # Kill the main app with a keyboard interrupt. Surpress the stderr as well.
                async def kill_main_app():
                    sys.stderr = None
                    raise KeyboardInterrupt("/exit was typed")
                print(
                    "Logging out of Discord. In unlikely cases, this can take a few seconds, please be patient...")
                main_loop.create_task(kill_main_app())
                main_loop.create_task(self.close())
        worker_loop = asyncio.new_event_loop()
        worker = Thread(target=start_thread, args=(worker_loop,))
        worker.start()
        worker_loop.call_soon_threadsafe(self.ui.get_user_input)

    # Start Message events
    async def on_message(self, message):
        await self.ui.update_messages(message.channel.id)

    async def on_message_edit(self, before, after):
        await self.ui.update_messages(after.channel.id)

    async def on_raw_message_edit(self, payload):
        await self.ui.update_messages(int(payload.data['channel_id']))

    async def on_message_delete(self, message):
        await self.ui.update_messages(message)

    async def on_raw_message_delete(self, payload):
        await self.ui.update_messages(int(payload.channel_id))
    # End Message events

    # Start User/Member events
    async def on_member_join(self, member):
        # Make sure the member's in the current channel's list (to avoid unnecessary updates)
        if member in self.ui.current_channel.members:
            self.ui.update_users_list()

    async def on_member_remove(self, member):
        # Make sure the member's in the current channel's list (to avoid unnecessary updates)
        if member in self.ui.current_channel.members:
            self.ui.update_users_list()

    async def on_member_update(self, before, after):
        # Make sure the member's in the current channel's list (to avoid unnecessary updates)
        if self.ui != None and after in self.ui.current_channel.members:
            self.ui.update_users_list()
            # Just in case it was a nickname change, update the messages too
            await self.ui.update_messages(self.ui.current_channel.id)
    # End User/Member events

    # Start Channel events
    async def on_guild_channel_update(self, before, after):
        self.ui.update_channels_list()

    async def on_guild_channel_create(self, channel):
        self.ui.update_channels_list()

    async def on_guild_channel_delete(self, channel):
        self.ui.update_channels_list()
    # End Channel events

    async def on_error(self, event, *args, **kargs):
        logger.write_to_log(f'{sys.exc_info()[1]}: {traceback.format_exc()}')
