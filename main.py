#!/usr/bin/python3
import discord

from os import error, get_terminal_size

from config import keys, config
from app import pycord_from_home


def okay_config_dimensions() -> tuple:
    """
    Makes sure all the config numbers make sense and that rules are matched. 

    Returns:
     tuple with: (okay_status, error_message)
     okay_status: If all configs check out, this is True. Otherwise False.
     error_message: if okay_status is False, this will have a message to dispaly to the user.
    """
    x, y = get_terminal_size()

    if config.MIN_CHAN_MESSAGE_WIDTH <= 0 or\
            config.CHAN_LIST_WIDTH <= 0 or\
            config.USER_LIST_WIDTH <= 0 or\
            config.MIN_WIN_WIDTH < 20 or\
            config.MIN_WIN_HEIGHT < 20:
        return (False, 'One or more configs are incorrect. Everything must be greater than 0 and minimums must be met.')

    if x < config.MIN_WIN_WIDTH or y < config.MIN_WIN_HEIGHT:
        return (False, f'Window size needs to be at LEAST {config.MIN_WIN_HEIGHT}x{config.MIN_WIN_WIDTH} in height x width')

    # 4 -> for separators
    if x < config.CHAN_LIST_WIDTH + config.USER_LIST_WIDTH + config.MIN_CHAN_MESSAGE_WIDTH + 4:
        return (False, 'Window width is too short to make configs draw correctly. Please adjust config widths.')

    # 9 for combination of separators
    if y < 9:
        return (False, 'Window height must be an absolute minimum of 9 no matter what.')

    if config.MIN_WIN_WIDTH < config.CHAN_LIST_WIDTH + config.USER_LIST_WIDTH + config.MIN_CHAN_MESSAGE_WIDTH + 4:
        return (False, 'Combined config widths are larger than the MIN_WIN_WIDTH. Please adjust config widths.')

    return (True, None)


if __name__ == "__main__":
    configs_okay, error_message = okay_config_dimensions()

    if not configs_okay:
        print(error_message)
        exit(-1)

    intents = discord.Intents().all()
    app = pycord_from_home.PycordFromHome(intents=intents)

    try:
        # app.run(keys.DISCORD_TOKEN, bot=False)
        app.run(keys.DISCORD_TOKEN)
    except KeyboardInterrupt:
        pass
