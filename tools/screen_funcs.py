import curses
import threading
from typing import Tuple

# Sets to true when it's in the middle of rendering. This prevents weird
# jumpiness when the user's typing or trying to do things while other stuff is updating.
rendering = threading.Lock()


def sc_addstr(screen: object, string: str, y_loc: int = -1, x_loc: int = -1, color_pair_number: int = 0) -> Tuple[int, int]:
    """
    A custom addstr for screen that locks a thread so it doesn't write to the screen multiple times. This
    helps with avoiding the screen from jumping around when the UI is trying to update things.

    Params:
      screen: The curses screen object
      string: The string to write out
      y_loc: The Y location to move the cursor to. -1 for x or y means no movement
      x_loc: The X location to move the cursor to. -1 for x or y means no movement
      color_pair_number: Optionally, The color to draw with.

    Returns:
      tuple with (y,x) for the screen positions after adding the string (but before moving it back to where it was)
    """
    with rendering:
        if y_loc == -1 or x_loc == -1:
            screen.addstr(string, curses.color_pair(color_pair_number))
            new_y, new_x = screen.getyx()
        else:
            y, x = screen.getyx()
            screen.move(y_loc, x_loc)
            screen.addstr(string, curses.color_pair(color_pair_number))
            new_y, new_x = screen.getyx()
            screen.move(y, x)
        return (new_y, new_x)


def sc_move(screen: object, y_loc: int, x_loc: int):
    """
    A custom move for screen that locks a thread so it doesn't write to the screen multiple times. This
    helps with avoiding the screen from jumping around when the UI is trying to update things.

    Params:
      screen: The curses screen object
      y_loc: The Y location to move the cursor to.
      x_loc: The X location to move the cursor to.
    """
    with rendering:
        screen.move(y_loc, x_loc)


def sc_refresh(screen: object):
    """
    A custom refresh for screen that locks a thread so it doesn't write to the screen multiple times. This
    helps with avoiding the screen from jumping around when the UI is trying to update things.
    """
    with rendering:
        screen.refresh()


def sc_getyx(screen: object) -> Tuple[int, int]:
    """
    A custom getyx() for screen that locks a thread so it doesn't write to the screen multiple times. This
    helps with avoiding the screen from jumping around when the UI is trying to update things.
    """
    with rendering:
        return screen.getyx()
