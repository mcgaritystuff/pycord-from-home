from datetime import datetime

from config import config


def write_to_log(message: str) -> None:
    with open(config.LOG_FILE, 'a') as f:
        f.write(f'{str(datetime.now())} - {message}\n')
